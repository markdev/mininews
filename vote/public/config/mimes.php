<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| MIME TYPES
| -------------------------------------------------------------------
| This file contains an array of mime types.  It is used by the
| Upload class to help identify allowed file types.
|
*/

$mimes = array(	'hqx'	=>	'public/mac-binhex40',
				'cpt'	=>	'public/mac-compactpro',
				'csv'	=>	array('text/x-comma-separated-values', 'text/comma-separated-values', 'public/octet-stream', 'public/vnd.ms-excel', 'public/x-csv', 'text/x-csv', 'text/csv', 'public/csv', 'public/excel', 'public/vnd.msexcel'),
				'bin'	=>	'public/macbinary',
				'dms'	=>	'public/octet-stream',
				'lha'	=>	'public/octet-stream',
				'lzh'	=>	'public/octet-stream',
				'exe'	=>	array('public/octet-stream', 'public/x-msdownload'),
				'class'	=>	'public/octet-stream',
				'psd'	=>	'public/x-photoshop',
				'so'	=>	'public/octet-stream',
				'sea'	=>	'public/octet-stream',
				'dll'	=>	'public/octet-stream',
				'oda'	=>	'public/oda',
				'pdf'	=>	array('public/pdf', 'public/x-download'),
				'ai'	=>	'public/postscript',
				'eps'	=>	'public/postscript',
				'ps'	=>	'public/postscript',
				'smi'	=>	'public/smil',
				'smil'	=>	'public/smil',
				'mif'	=>	'public/vnd.mif',
				'xls'	=>	array('public/excel', 'public/vnd.ms-excel', 'public/msexcel'),
				'ppt'	=>	array('public/powerpoint', 'public/vnd.ms-powerpoint'),
				'wbxml'	=>	'public/wbxml',
				'wmlc'	=>	'public/wmlc',
				'dcr'	=>	'public/x-director',
				'dir'	=>	'public/x-director',
				'dxr'	=>	'public/x-director',
				'dvi'	=>	'public/x-dvi',
				'gtar'	=>	'public/x-gtar',
				'gz'	=>	'public/x-gzip',
				'php'	=>	'public/x-httpd-php',
				'php4'	=>	'public/x-httpd-php',
				'php3'	=>	'public/x-httpd-php',
				'phtml'	=>	'public/x-httpd-php',
				'phps'	=>	'public/x-httpd-php-source',
				'js'	=>	'public/x-javascript',
				'swf'	=>	'public/x-shockwave-flash',
				'sit'	=>	'public/x-stuffit',
				'tar'	=>	'public/x-tar',
				'tgz'	=>	array('public/x-tar', 'public/x-gzip-compressed'),
				'xhtml'	=>	'public/xhtml+xml',
				'xht'	=>	'public/xhtml+xml',
				'zip'	=>  array('public/x-zip', 'public/zip', 'public/x-zip-compressed'),
				'mid'	=>	'audio/midi',
				'midi'	=>	'audio/midi',
				'mpga'	=>	'audio/mpeg',
				'mp2'	=>	'audio/mpeg',
				'mp3'	=>	array('audio/mpeg', 'audio/mpg', 'audio/mpeg3', 'audio/mp3'),
				'aif'	=>	'audio/x-aiff',
				'aiff'	=>	'audio/x-aiff',
				'aifc'	=>	'audio/x-aiff',
				'ram'	=>	'audio/x-pn-realaudio',
				'rm'	=>	'audio/x-pn-realaudio',
				'rpm'	=>	'audio/x-pn-realaudio-plugin',
				'ra'	=>	'audio/x-realaudio',
				'rv'	=>	'video/vnd.rn-realvideo',
				'wav'	=>	array('audio/x-wav', 'audio/wave', 'audio/wav'),
				'bmp'	=>	array('image/bmp', 'image/x-windows-bmp'),
				'gif'	=>	'image/gif',
				'jpeg'	=>	array('image/jpeg', 'image/pjpeg'),
				'jpg'	=>	array('image/jpeg', 'image/pjpeg'),
				'jpe'	=>	array('image/jpeg', 'image/pjpeg'),
				'png'	=>	array('image/png',  'image/x-png'),
				'tiff'	=>	'image/tiff',
				'tif'	=>	'image/tiff',
				'css'	=>	'text/css',
				'html'	=>	'text/html',
				'htm'	=>	'text/html',
				'shtml'	=>	'text/html',
				'txt'	=>	'text/plain',
				'text'	=>	'text/plain',
				'log'	=>	array('text/plain', 'text/x-log'),
				'rtx'	=>	'text/richtext',
				'rtf'	=>	'text/rtf',
				'xml'	=>	'text/xml',
				'xsl'	=>	'text/xml',
				'mpeg'	=>	'video/mpeg',
				'mpg'	=>	'video/mpeg',
				'mpe'	=>	'video/mpeg',
				'qt'	=>	'video/quicktime',
				'mov'	=>	'video/quicktime',
				'avi'	=>	'video/x-msvideo',
				'movie'	=>	'video/x-sgi-movie',
				'doc'	=>	'public/msword',
				'docx'	=>	array('public/vnd.openxmlformats-officedocument.wordprocessingml.document', 'public/zip'),
				'xlsx'	=>	array('public/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'public/zip'),
				'word'	=>	array('public/msword', 'public/octet-stream'),
				'xl'	=>	'public/excel',
				'eml'	=>	'message/rfc822',
				'json' => array('public/json', 'text/json')
			);


/* End of file mimes.php */
/* Location: ./public/config/mimes.php */
