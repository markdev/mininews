﻿#bc
# mininews-迷你新闻
    > Created 2015 by MarkDing <mark@ezve.com>
    > 该项目作为团队协作尝试性项目。

## 公告
    成员开发环境配置好后，请更新该文档。

## 团队成员
    MARK <mark@ezve.com>
    TXT <222275@qq.com>
    LYF <281064113@qq.com>
    BC <95700950@qq.com>
    LF <447310975@qq.com>

## 项目描述
    管理员在后台配置新闻分类，网站设置等。
    小编在后台编辑新闻，上传图片，发布新闻。
    用户通过电脑或手机浏览器访问主页可以查看推荐新闻，进入分类频道浏览新闻。
    用户注册登录后可对新闻点赞，可点评。
    主编可以在后台查看新闻浏览排行，可对点评进行回复，可以删除或隐藏点评。

## 需求分析
此处由LF编写。

## 界面设计
此处由BC编写。

## 系统设计
此处由TXT,LYF编写。
报到 by lyf lf bicheng

## 工具软件
* [FBT-MININEWS](http://git.oschina.net/markdev/mininews)
* [tortoisegit](http://tortoisegit.org/)
* [Notepad++](http://notepad-plus-plus.org/)
* [WAMPSERVER](http://www.wampserver.com/en/)
* xxxx
* 11111111111111111
